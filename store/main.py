from flask import render_template, redirect, request, send_file
from forms import AddProductForm
import io
from settings import app, db
from models import Product


@app.route('/')
def index():
    products = Product.query.all()
    return render_template('index.html', products=products)


@app.route('/add', methods=['POST', 'GET'])
def add_product():
    form = AddProductForm(request.form)
    if request.method == 'POST' and form.validate():
        image_file = request.files['image']
        title = form.title.data
        category = form.category.data
        price = form.price.data
        products = Product(
                        title=title, category=category, 
                        price=price, img=image_file.read()
                    )
        db.session.add(products)
        db.session.commit()
        return redirect('/')

    return render_template('add.html', form=form)


@app.route('/del/<int:pk>')
def delete_product(pk):
    products = Product.query.get_or_404(pk)
    try:
        db.session.delete(products)
        db.session.commit()
        return redirect('/')
    except:
        return 'При удалении товара произошла ошибка'


@app.route('/get_image/<int:pk>')
def get_image(pk):
    parts = Product.query.get(pk)
    image_data = parts.img
    image_buffer = io.BytesIO()
    image_buffer.write(image_data)
    image_buffer.seek(0)
    return send_file(image_buffer, mimetype='image/jpeg')


if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.run(debug=True, host="127.0.0.1", port=5433)
