from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField
from flask_wtf.file import FileField
from wtforms.validators import Length, DataRequired
import re
class AddProductForm(FlaskForm):
	title = StringField("Название товара: ", validators=[Length(min=4, max=50)])
	price = IntegerField("Цена товара: ", validators=[DataRequired()])
	category = StringField("Категория товара: ", validators=[Length(min=4, max=50)])
	image = FileField('Image File')
	submit = SubmitField("Добавить товар")

	def validate_image(form, field):
		if field.data:
			field.data = re.sub(r'[^a-z0-9_.-]', '_', field.data)

