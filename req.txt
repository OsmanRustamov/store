Flask==2.3.2
psycopg2-binary==2.9.6
WTForms==3.0.1
Flask-WTF==1.1.1
Flask-SQLAlchemy==3.0.3
Flask-Migrate==4.0.4